# RamoS jigsaw puzzle
### Author: Roman Shchur.

*This is mainly pet project as well as DevCom testing project*  

Project is application for jigsaw puzzle.  

***Warning!***  
*Last changes and features are staged in Development branch, as it is still not release ready version.*  

**Features:**  
- [x] Windowed UI;  
- [x] Draggable elements;  
- [x] Difficulty selection;  
- [x] Custom image selection;  
- [x] Image cutting;  
- [x] Game ending dialog;  
- [x] Image rotation *(Not yet compatible with auto collector);*  
- [x] Auto collector *(Still WIP).*  

**Not yet:**  
- [ ] Auto collector debug;  
- [ ] Auto collector finish *(It mostly stops by StackOverflowError or on second piece).*  
- [ ] Auto collector-image rotation bridge;    
