package jigsawPuzzle.appUserInterface;

import jigsawPuzzle.appUserInterface.customElements.DraggableJComponentImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import jigsawPuzzle.logic.ImageCutter;
import jigsawPuzzle.logic.ImageResizer;
import sun.awt.image.ToolkitImage;

public class MainPage extends JFrame {
    private JPanel mainPanel;
    private JTabbedPane tabbedPane1;
    private JPanel gamePanel;
    private JPanel imagePanel;
    private JPanel piecesPanel;

    private Image image;
    private BufferedImage imagePieces[];
    private int difficulty;

    public MainPage(int difficulty, Image image, int imgWidth, int imgHeight) throws HeadlessException {
        super();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setUndecorated(false);
        this.setVisible(true);
        this.setContentPane(mainPanel);
        this.pack();

        ImageResizer imageResizer = new ImageResizer();
        this.image = imageResizer.resizeImage(image, imgWidth, imgHeight);
        this.difficulty = difficulty;
        initializeElements();
    }
    void initializeElements() {
        piecesPanel.setLayout(null);
        imagePanel.setVisible(true);
        imagePanel.setLayout(new BoxLayout(imagePanel, BoxLayout.PAGE_AXIS));
        try {
            Image myPicture = this.image;
            JLabel label1 = new JLabel(new ImageIcon(myPicture));
            imagePanel.add(label1);
            ImageCutter imageCutter = new ImageCutter();
            switch (this.difficulty) {
                case 0:
                    this.imagePieces = imageCutter.cutImage(myPicture, 5, 3);
                    break;
                case 1:
                    this.imagePieces = imageCutter.cutImage(myPicture, 8, 5);
                    break;
                case 2:
                    this.imagePieces = imageCutter.cutImage(myPicture, 10, 8);
                    break;
                case 3:
                    this.imagePieces = imageCutter.cutImage(myPicture, 20, 10);
                    break;
                case 4:
                    this.imagePieces = imageCutter.cutImage(myPicture, 100, 50);
                    break;
                default:
                    System.out.println("Looks like something went wrong with difficulty selection.");
                    JOptionPane.showMessageDialog(mainPanel, "What ever, take fastest game.", "Looks like something went wrong", JOptionPane.PLAIN_MESSAGE);
                    this.imagePieces = imageCutter.cutImage(myPicture, 5, 3);
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (BufferedImage image : imagePieces) {
            DraggableJComponentImage dc = null;
            try {
                dc = new DraggableJComponentImage(image);
                dc.setVisible(true);
                dc.setLocation(randomInRange(0, 600), randomInRange(0, 400));
            } catch (IOException e) {
                e.printStackTrace();
            }
            piecesPanel.add(dc);
        }
    }
    private int randomInRange(int min, int max) {
        Random random = new Random();
        return random.nextInt(max - min) + min;
    }
}
