package jigsawPuzzle.appUserInterface;

import jigsawPuzzle.logic.ImageFilter;
import sun.awt.image.ToolkitImage;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class SettingsPage extends JFrame {
    private JPanel mainSettingsPanel;
    private JPanel difficultyPanel;
    private JPanel imagePanel;
    private JRadioButton fastOprion;
    private JRadioButton normalOption;
    private JRadioButton longOption;
    private JRadioButton eternityOption;
    private JButton selectImageButton;
    private JButton startButton;
    private JRadioButton veryFastOprion;

    private int difficulty;
    private Image image;
    private ToolkitImage image2;
    private int imgHeight;
    private int imgWidth;

    public SettingsPage() {
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setUndecorated(false);
        this.setVisible(true);
        this.setContentPane(mainSettingsPanel);
        this.pack();

        veryFastOprion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficulty = 0;
            }
        });
        fastOprion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficulty = 1;
            }
        });
        normalOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficulty = 2;
            }
        });
        longOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficulty = 3;
            }
        });
        eternityOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                difficulty = 4;
            }
        });

        selectImageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    image = selectImage();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startGame();
            }
        });
    }
    private Image selectImage() throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(new ImageFilter());
        fileChooser.showOpenDialog(this);
        File file = fileChooser.getSelectedFile();
        BufferedImage image = ImageIO.read(file);
        this.imgWidth = image.getWidth();
        this.imgHeight = image.getHeight();
        return image;
    }
    private void startGame () {
        this.dispose();
        new MainPage(this.difficulty, this.image, this.imgWidth, this.imgHeight);
    }
    private void createUIComponents() {
    }
}
