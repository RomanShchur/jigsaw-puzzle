package jigsawPuzzle.appUserInterface.customElements;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class DraggableJComponentImage extends DraggableJComponent implements ImageObserver {
    protected BufferedImage image;
    private boolean autoSize = false;
    private Dimension autoSizeDimension;

    public DraggableJComponentImage(BufferedImage image) throws IOException {
        super();
        setLayout(null);
        setBackground(Color.black);
//        FileInputStream fis = new FileInputStream(file);
//        BufferedImage image = ImageIO.read(fis);
        this.image = image;
        this.setSize(image.getWidth(), image.getHeight());
        this.autoSizeDimension = new Dimension(image.getWidth(), image.getHeight());
    }
    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.clearRect(0, 0, image.getWidth(), image.getHeight());
        if (image != null) {
            g2d.drawImage(image, 0, 0, image.getWidth(), image.getHeight(), this);
        } else {
            g2d.setColor(getBackground());
            g2d.fillRect(0, 0, image.getWidth(), image.getHeight());
        }
    }
    public boolean imageUpdate(Image img, int infoflags, int x, int y, int w, int h) {
        if (infoflags == ALLBITS) {
            repaint();
            return false;
        }
        return true;
    }
}
