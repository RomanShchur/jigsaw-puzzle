package jigsawPuzzle.logic;

import sun.awt.image.ToolkitImage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.awt.*;
import java.util.ArrayList;

public class ImageCutter {
    private BufferedImage image2;
    private int rows;
    private int cols;

    public BufferedImage[] cutImage(Image image, int rows, int cols) throws IOException {
        this.image2 = ((ToolkitImage) image).getBufferedImage();
        this.rows = rows;
        this.cols = cols;
        int pieces = rows * cols;
        int pieceWidth = image2.getWidth() / cols;
        int pieceHeight = image2.getHeight() / rows;
        int count = 0;
        BufferedImage imagePieces[] = new BufferedImage[pieces];
        for (int x = 0; x < rows; x++) {
            for (int y = 0; y < cols; y++) {
                imagePieces[count] = new BufferedImage(pieceWidth, pieceHeight, image2.getType());
                Graphics2D gr = imagePieces[count++].createGraphics();
                gr.drawImage(image, 0, 0, pieceWidth, pieceHeight, pieceWidth * y, pieceHeight * x, pieceWidth * y + pieceWidth, pieceHeight * x + pieceHeight, null);
                gr.dispose();
            }
        }
        return imagePieces;
    }
}