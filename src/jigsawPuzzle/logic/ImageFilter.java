package jigsawPuzzle.logic;

import javax.swing.filechooser.FileFilter;
import java.io.File;

public class ImageFilter extends FileFilter {
    final static String[] availableExtensions = {"jpeg", "jpg", "gif", "tif", "tiff", "png"};
    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        String extension = getExtension(f);
        if (extension != null) {
            if (extension.equals(availableExtensions[0]) ||
                    extension.equals(availableExtensions[1]) ||
                    extension.equals(availableExtensions[2]) ||
                    extension.equals(availableExtensions[3]) ||
                    extension.equals(availableExtensions[4]) ||
                    extension.equals(availableExtensions[5])) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
    @Override
    public String getDescription() {
        return "Image Only";
    }
    String getExtension(File f) {
        String ext = null;
        String s = f.getName();
        int i = s.lastIndexOf('.');
        if (i > 0 &&  i < s.length() - 1) {
            ext = s.substring(i+1).toLowerCase();
        }
        return ext;
    }
}

