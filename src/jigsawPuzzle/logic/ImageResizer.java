package jigsawPuzzle.logic;

import sun.awt.image.ToolkitImage;

import java.awt.*;
import java.awt.image.BufferedImage;

public class ImageResizer {
    public Image resizeImage (Image oldImage, int imgWidth, int imgHeight){
        Image newImage;
        int panelWidth = 1200, panelHeight = 800;
        double scale;
        double imgWidth2 = imgWidth;
        double imgHeight2 = imgHeight;
//      if image is wider than higher scale to fit width
        if(panelWidth/imgWidth2 < panelHeight/imgHeight2){
            scale = panelWidth/imgWidth2;
            newImage = oldImage.getScaledInstance((int) (scale*imgWidth2), (int) (scale*imgHeight2), Image.SCALE_SMOOTH);
        }
//      if image is higher than wider scale to fit height
        else if (panelWidth/imgWidth2 > panelHeight/imgHeight2){
            scale = panelHeight/imgHeight2;
            newImage = oldImage.getScaledInstance((int) (scale*imgWidth2), (int) (scale*imgHeight2), Image.SCALE_SMOOTH);
        }
//      if image is square fit height as smaller dimension
        else if (panelWidth/imgWidth2 == panelHeight/imgHeight2){
            scale = panelHeight/imgHeight2;
            newImage = oldImage.getScaledInstance((int) (scale*imgWidth2), (int) (scale*imgHeight2), Image.SCALE_SMOOTH);
        }
//      in unknown situation just squeeze it to that dimensions
        else {
            newImage = oldImage.getScaledInstance(1200, 800, Image.SCALE_AREA_AVERAGING);
        }
        return newImage;
    }
}
